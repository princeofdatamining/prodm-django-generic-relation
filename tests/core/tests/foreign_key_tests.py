from django.test import TestCase

from django.db import models
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.models import ContentType

from generic_relation.related_foreign import RelatedForeignKey
from core.models import Item

class RelatedForeignTest(TestCase):

    def setUp(self):
        self.ct_u = ContentType.objects.get_for_model(User)
        self.ct_g = ContentType.objects.get_for_model(Group)
        #
        RelatedForeignKey(User, cache_name='content', on_delete=models.CASCADE).contribute_to_class(Item, 'content_user')
        RelatedForeignKey(Group,cache_name='content', on_delete=models.CASCADE).contribute_to_class(Item, 'content_group')
        #
        self.u1, _ = User.objects.get_or_create(username='u01')
        self.u2, _ = User.objects.get_or_create(username='u02')
        self.tu, _ = Item.objects.get_or_create(scene='User', name='Family', content_type=self.ct_u, object_id=self.u1.pk)
        Item.objects.get_or_create(scene='User', name='Friend', content_type=self.ct_u, object_id=self.u1.pk)
        Item.objects.get_or_create(scene='X', name='Friend', content_type=self.ct_u, object_id=self.u1.pk)
        Item.objects.get_or_create(scene='X', name='Family', content_type=self.ct_u, object_id=self.u2.pk)
        #
        self.g1, _ = Group.objects.update_or_create(defaults=dict(name='Admin'), pk=self.u1.pk)
        self.tg, _ = Item.objects.get_or_create(scene='Group', name='sudo', content_type=self.ct_g, object_id=self.g1.pk)

    def test(self):
        self.assertIsInstance(self.tu.content_user, User)
        self.assertEqual(self.tu.content_user, self.u1)
        self.assertEqual(None, self.tu.content_group)
        #
        self.assertIsInstance(self.tg.content_group, Group)
        self.assertEqual(self.tg.content_group, self.g1)
        self.assertEqual(None, self.tg.content_user)
        # 
        self.assertEqual(3, len(Item.objects.filter(content_user=self.u1, content_type=self.ct_u)))
        self.assertEqual(1, len(Item.objects.filter(content_user=self.u1, content_type=self.ct_u, scene='X')))
        self.assertEqual(2, len(Item.objects.filter(content_user=self.u1, content_type=self.ct_u, name='Friend')))
        ''' TODO: 指定`contnt_user`的时候，内部直接对`content_type`做限定
        self.assertEqual(3, len(Item.objects.filter(content_user=self.u1)))
        self.assertEqual(1, len(Item.objects.filter(content_user=self.u1, scene='X')))
        self.assertEqual(2, len(Item.objects.filter(content_user=self.u1, name='Friend')))
        '''
