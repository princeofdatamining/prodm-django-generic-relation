from django.test import TestCase

from django.db import models
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.models import ContentType

from generic_relation.reverse_generic import GenericOneToOne, GenericOneToMany
from core.models import Item

class ReverseGenericTest(TestCase):

    def setUp(self):
        self.ct_u = ContentType.objects.get_for_model(User)
        self.ct_g = ContentType.objects.get_for_model(Group)
        #
        GenericOneToOne (Item, related_val_field='name', extra_filters=dict(scene='owner')).contribute_to_class(Group,'owner')
        GenericOneToOne (Item, related_val_field='name', extra_filters=dict(scene='owner')).contribute_to_class(User, 'owner')
        GenericOneToMany(Item, related_val_field='name', extra_filters=dict(scene='tagged')).contribute_to_class(User, 'tagged')
        #
        self.u1, _ = User.objects.get_or_create(username='u01')
        self.u2, _ = User.objects.get_or_create(username='u02')
        self.g1, _ = Group.objects.update_or_create(defaults=dict(name='Admin'), pk=self.u1.pk)

    def check_result(self, keys, content=None, **kwargs):
        if content:
            kwargs['content_type'] = ContentType.objects.get_for_model(content)
            kwargs['object_id'] = content.pk
        results = Item.objects.filter(**kwargs)
        for row in results:
            self.assertIn(row.name, keys)
        self.assertEqual(len(keys), len(results))

    def test(self):
        self.g1.owner = None
        self.u1.owner, self.u1.tagged = None, []
        self.u2.owner, self.u2.tagged = None, []
        self.check_result([], content=self.g1, scene='owner')
        self.check_result([], content=self.u1, scene='owner')
        self.check_result([], content=self.u2, scene='owner')
        self.check_result([], content=self.u1, scene='tagged')
        self.check_result([], content=self.u2, scene='tagged')
        #
        self.g1.owner = g = 'OG'
        self.check_result([g], content=self.g1, scene='owner')
        self.check_result([], content=self.u1, scene='owner')
        self.check_result([], content=self.u2, scene='owner')
        self.check_result([], content=self.u1, scene='tagged')
        self.check_result([], content=self.u2, scene='tagged')
        #
        self.g1.owner = g = 'OG1'
        self.u1.owner = o1 = 'OU1'
        self.check_result([g], content=self.g1, scene='owner')
        self.check_result([o1], content=self.u1, scene='owner')
        self.check_result([], content=self.u2, scene='owner')
        self.check_result([], content=self.u1, scene='tagged')
        self.check_result([], content=self.u2, scene='tagged')
        #
        self.g1.owner = None
        self.u2.owner = o2 = 'OU2'
        self.check_result([], content=self.g1, scene='owner')
        self.check_result([o1], content=self.u1, scene='owner')
        self.check_result([o2], content=self.u2, scene='owner')
        self.check_result([], content=self.u1, scene='tagged')
        self.check_result([], content=self.u2, scene='tagged')
        #
        self.u1.tagged = x = ['T1', 'T11', 'T111']
        self.u2.tagged = y = ['T2', 'T22', 'T222']
        self.check_result([], content=self.g1, scene='owner')
        self.check_result([o1], content=self.u1, scene='owner')
        self.check_result([o2], content=self.u2, scene='owner')
        self.check_result(x, content=self.u1, scene='tagged')
        self.check_result(y, content=self.u2, scene='tagged')
        #
        self.u1.tagged = x = ['T1', 'T1111']
        self.u2.tagged = y = ['Tz', 'Tzz', 'Tzzz', 'Tzzzz']
        self.check_result([], content=self.g1, scene='owner')
        self.check_result([o1], content=self.u1, scene='owner')
        self.check_result([o2], content=self.u2, scene='owner')
        self.check_result(x, content=self.u1, scene='tagged')
        self.check_result(y, content=self.u2, scene='tagged')
        #
        self.u1.tagged = x = []
        self.u2.tagged = y = []
        self.check_result([], content=self.g1, scene='owner')
        self.check_result([o1], content=self.u1, scene='owner')
        self.check_result([o2], content=self.u2, scene='owner')
        self.check_result(x, content=self.u1, scene='tagged')
        self.check_result(y, content=self.u2, scene='tagged')
