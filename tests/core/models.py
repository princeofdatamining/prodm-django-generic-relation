from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

class Item(models.Model):

    class Meta:
        verbose_name = verbose_name_plural = 'Tagged Item'

    # option = models.ForeignKey(Option)
    scene = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    #
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(max_length=191)
    content = GenericForeignKey()

    def __str__(self): return self.name
