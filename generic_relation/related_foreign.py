from django.db.models.fields.related import RelatedField, ForeignKey
from django.db.models.fields.reverse_related import ManyToOneRel
from django.db.models.fields.related_descriptors import ForwardManyToOneDescriptor
from django.contrib.contenttypes.models import ContentType
from django.utils.functional import cached_property
import django

class RelatedToOneRel(ManyToOneRel):

    def get_cache_name(self):
        # 直接用`GenericForeignKey`的`cache_name`
        return self.field._cache_name

class RelatedForeignKey(ForeignKey):

    rel_class = RelatedToOneRel

    def __init__(self, *args, **kwargs):
        self._extra_filters = kwargs.pop('extra_filters', None)
        self._cache_name = kwargs.pop('cache_name')
        self.content_type_field_name = kwargs.pop('content_type_field_name', 'content_type')
        self.object_id_field_name = kwargs.pop('object_id_field_name', 'object_id')
        super(RelatedForeignKey, self).__init__(*args, **kwargs)

    def contribute_to_class(self, cls, name, **kwargs):
        kwargs['virtual_only'] = True
        super(RelatedField, self).contribute_to_class(cls, name, **kwargs)
        self.opts = cls._meta
        setattr(cls, self.name, RelatedManyToOneDescriptor(self.remote_field))

    def resolve_related_fields(self):
        # model.object_id <==> remote.pk
        return [(self.model._meta.get_field(self.object_id_field_name), self.remote_field.model._meta.pk)]

    def get_col(self, alias, output_field=None):
        # 用`object_id`来代替
        f = self.model._meta.get_field(self.object_id_field_name)
        from django.db.models.expressions import Col
        return Col(alias, f, output_field or f)

    ''' TODO: filter with extra content_type restriction
        Tagged.filter(user=do.u1)
        # Now:
        # FROM "tagged_item" WHERE "tagged_item"."object_id" = ?
        ###
        # Expect:
        # FROM "tagged_item" WHERE "tagged_item"."object_id" = ? AND "tagged_item"."content_type_id" = ?
    '''

    def get_extra_restriction(self, where_class, alias, related_alias):
        cond = where_class()
        # TODO: if filter with extra implemented, remove following 3 lines
        field = self.model._meta.get_field(self.content_type_field_name)
        lookup = field.get_lookup('exact')(field.get_col(related_alias), self.content_type.pk)
        cond.add(lookup, 'AND')
        # 是否需要额外的限制条件
        for field_name, field_value in self.extra_filters.items():
            field = self.model._meta.get_field(field_name)
            lookup = field.get_lookup('exact')(field.get_col(related_alias), field_value)
            cond.add(lookup, 'AND')
        #
        return cond

    ###

    @cached_property
    def content_type(self):
        return ContentType.objects.get_for_model(self.remote_field.model,
                                                 for_concrete_model=True)

    @cached_property
    def extra_filters(self):
        while not isinstance(self._extra_filters, dict):
            if callable(self._extra_filters):
                self._extra_filters = self._extra_filters()
            else:
                self._extra_filters = {}
        return self._extra_filters

class RelatedManyToOneDescriptor(ForwardManyToOneDescriptor):

    # 加上类型判定

    if django.VERSION >= (1, 10):
        def __get__(self, instance, cls=None):
            if instance is None:
                return self
            return self.result_as_model(
                super(RelatedManyToOneDescriptor, self).__get__(instance, cls=None)
            )
    else:
        def __get__(self, instance, instance_type=None):
            if instance is None:
                return self
            return self.result_as_model(
                super(RelatedManyToOneDescriptor, self).__get__(instance, instance_type=None)
            )

    def result_as_model(self, result):
        if not result:
            return result
        if not isinstance(result, self.field.model):
            return None
        return result

    def __set__(self, instance, value):
        if value and not isinstance(value, self.field.related_model):
            raise ValueError(
                'Cannot assign "{}" to "{}".'.format(
                    value.__class__.__name__, 
                    self.field.related_model.__name__,
                )
            )
        super(RelatedManyToOneDescriptor, self).__set__(instance, value)
