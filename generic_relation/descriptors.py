import django
from django.utils.functional import cached_property
from django.db.models.fields.related_descriptors import *
from django.contrib.contenttypes.fields import create_generic_related_manager

class ReverseGenericDescriptor(ReverseManyToOneDescriptor):

    @cached_property
    def related_manager_cls(self):
        return create_reverse_generic_manager(
            self.rel.model._default_manager.__class__,
            self.rel, self.one_to_one,
        )

    @cached_property
    def cache_name(self):
        return self.field.get_cache_name()

    # return instance, dirty ## dirty for save()
    def get_or_new_related(self, instance, new_obj, original):
        if isinstance(new_obj, self.rel.model):
            return new_obj, True
        if callable(self.field._related_converter):
            return self.field._related_converter(instance, new_obj, original)
        if not self.field._related_val_field:
            return new_obj, True
        for raw_obj in original:
            if new_obj == getattr(raw_obj, self.field._related_val_field):
                return raw_obj, False
            setattr(raw_obj, self.field._related_val_field, new_obj)
            return raw_obj, True
        values = self.field.extra_filters.copy()
        values[self.field._related_val_field] = new_obj
        values[self.field.content_type_field_name] = self.field.get_content_type()
        values[self.field.object_id_field_name] = instance.pk
        return self.rel.model(**values), True

#

class ReverseGenericOneToOneDescriptor(ReverseGenericDescriptor):

    one_to_one = True
    def __get__(self, instance, **kwargs):
        if instance is None:
            return self
        try:
            rel_obj = getattr(instance, self.cache_name)
        except AttributeError:
            queryset = self.related_manager_cls(instance)
            rel_obj = queryset.first()
            setattr(instance, self.cache_name, rel_obj)
        return rel_obj

    def __set__(self, instance, value):
        rel_obj = self.__get__(instance)
        if value is None:
            new_obj = None
            rel_obj and rel_obj.delete()
        else:
            new_obj, dirty = self.get_or_new_related(instance, value, [rel_obj] if rel_obj else [])
            dirty and new_obj.save()
        setattr(instance, self.cache_name, new_obj)

class ReverseGenericManyToOneDescriptor(ReverseGenericDescriptor):
    
    one_to_one = False

    def __set__(self, instance, value):
        if not callable(self.field._related_converter) and not self.field._related_val_field:
            return super(ReverseGenericManyToOneDescriptor, self).__set__(instance, value)
        manager = self.__get__(instance)
        original = [e for e in manager.all()]
        updated = []
        inserted = []
        if not isinstance(value, (list, tuple)):
            value = [value]
        for v in value:
            new_obj, dirty = self.get_or_new_related(instance, v, original)
            if not new_obj.pk:
                inserted.append(new_obj)
            else:
                for i, raw_obj in enumerate(original):
                    if raw_obj.pk == new_obj.pk:
                        original.pop(i)
                        break
                dirty and updated.append(new_obj)
        for new_obj in inserted:
            if original:
                new_obj.pk = original.pop().pk
                updated.append(new_obj)
        for e in updated:
            e.save()
        for e in inserted:
            e.pk or e.save()
        for e in original:
            e.delete()

#

def create_reverse_generic_manager(manager, rel, one_to_one):

    GenericRelatedManager = create_generic_related_manager(manager, rel)

    class ReverseGenericManager(GenericRelatedManager):

        def __init__(self, instance=None):
            super(ReverseGenericManager, self).__init__(instance=instance)
            self.core_filters.update(rel.field.extra_filters)

        def __call__(self, **kwargs):
            manager = getattr(self.model, kwargs.pop('manager'))
            manager_class = create_reverse_generic_manager(manager_class.__class__, rel, one_to_one)
            return manager_class(instance=self.instance)
        do_not_call_in_templates = True

    return ReverseGenericManager
