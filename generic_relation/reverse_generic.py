from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from .descriptors import *

from django.utils.encoding import smart_text
from django.utils.functional import cached_property

class GenericForeignObject(GenericRelation):

    def __init__(self, *args, **kwargs):
        self._extra_filters = kwargs.pop('extra_filters', {})
        self._related_val_field = kwargs.pop('related_val_field', None)
        self._related_converter = kwargs.pop('related_converter', None)
        super(GenericForeignObject, self).__init__(*args, **kwargs)

    def contribute_to_class(self, cls, name, **kwargs):
        kwargs['virtual_only'] = True
        #### Note: use ForeignObject, not GenericForeignObject
        super(GenericRelation, self).contribute_to_class(cls, name, **kwargs)
        self.model = cls
        setattr(cls, self.name, self.reverse_descriptor_class(self.remote_field))

    def get_extra_restriction(self, where_class, alias, remote_alias):
        cond = super(GenericForeignObject, self).get_extra_restriction(where_class, alias, remote_alias)
        for field_name, field_value in self.extra_filters.items():
            field = self.remote_field.model._meta.get_field(field_name)
            lookup = field.get_lookup('exact')(field.get_col(remote_alias), field_value)
            cond.add(lookup, 'AND')
        return cond

    @cached_property
    def extra_filters(self):
        while not isinstance(self._extra_filters, dict):
            if callable(self._extra_filters):
                self._extra_filters = self._extra_filters()
            else:
                self._extra_filters = {}
        return self._extra_filters

class GenericOneToOne(GenericForeignObject):

    one_to_many = False
    one_to_one = True
    reverse_descriptor_class = ReverseGenericOneToOneDescriptor

    def get_internal_type(self):
        return "OneToOneField"

    def value_to_string(self, obj):
        instance = getattr(obj, self.name)
        return instance._get_pk_val() if instance else None

class GenericOneToMany(GenericForeignObject):

    one_to_many = True
    one_to_one = False
    reverse_descriptor_class = ReverseGenericManyToOneDescriptor

    def get_internal_type(self):
        return "ManyToManyField"

    def value_to_string(self, obj):
        qs = getattr(obj, self.name).all()
        return smart_text([instance._get_pk_val() for instance in qs])

#
