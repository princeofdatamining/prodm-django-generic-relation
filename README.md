## django-generic-relation

[![Build Status](https://travis-ci.org/princeofdatamining/django-generic-relation.png?branch=master)](https://travis-ci.org/princeofdatamining/django-generic-relation)

`django-generic-relation` is a Django appliation and library for `GenericForeignKey` and `GenericRelaton` of `django.contrib.contenttypes.fields`.

## Sample Models

    class Tagged(models.Model):
        scene = CharField
        name = CharField
        content_type = ForeignKey(ContentType)
        object_id = CharField()
        content = GenericForeignKey()


## GenericOneToMany

Like `django.contrib.conetnttypes.fields.GenericRelaton`.

`__init__`(...)

* `extra_filters` for extra values of `insert` or filters of `select`.

* `related_converter` for convert value to related model instance.

* `related_val_fild` for `__set__`

Sample:

    tags = GenericOneToMany(Tagged, related_val_fiedl='name', extra_filters=dit(scene='tags'))
    tags.contribute_to_class(User, 'tags')
    #
    user.tags = ['Python', 'Django']
    print(*(t.name for t in user.tags.all()))
    User.objects.filter(tags__name='Django')
    #
    user.tags = []


## GenericOneToOne

Like `GenericOneToMany`, but `__get__` or `__set__` work with only one related.

Sample:

    country = GenericOneToOne(Tagged, related_val_field='name', extra_filters=dict(scene='country'))
    country.contribute_to_class(User, 'country')
    #
    user.country = 'USA'
    print(user.country)
    User.objects.filter(country__name='USA')
    #
    user.country = None


## RelatedForeignKey

Sample:

    RelatedForeignKey(User, cache_name='content', on_delete=models.CASCADE).contribute_to_class(Tagged, 'user')
    RelatedForeignKey(Group,cache_name='content', on_delete=models.CASCADE).contribute_to_class(Tagged, 'group')
    #
    Tagged.filter(user=u1)
    # SELECT ... FROM "tagged_item" WHERE "tagged_item"."object_id" = 1
    ### TODO: perfect WHERE is with content_type_id restriction
    # SELECT ... FROM "tagged_item" 
    # INNER JOIN user ON taggedt_item.object_id = user.id AND tagged_item.content_type_id = ?
    # WHERE ...

    Tagged.filter(user__username='?')
    # SELECT ... FROM "tagged_item" 
    # INNER JOIN "auth_user" ON ("tagged_item"."object_id" = "auth_user"."id" AND ("tagged_item"."content_type_id" = 4)) 
    # WHERE "auth_user"."username" = '?'
